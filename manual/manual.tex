% Created 2020-09-02 mié 10:22
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{minted}
\author{David Pineda @ CSN-Geodesia}
\date{Junio 2020}
\title{Sistema GNSS Collector}
\hypersetup{
 pdfauthor={David Pineda @ CSN-Geodesia},
 pdftitle={Sistema GNSS Collector},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 27.1 (Org mode 9.3.7)}, 
 pdflang={English}}
\begin{document}

\maketitle
\tableofcontents


\section{Versión}
\label{sec:org64a14a6}

Manual GNSS Collector V.1.0.0

\section{Introducción}
\label{sec:orge1a408d}

El sistema \textbf{GNSS Collector} es un servicio que debe operar en un servidor \textbf{Gnu/Linux}.

Realiza la adquisición en tiempo real los datos de toda una red de sensores
\textbf{GNSS} y los almacena en una base de datos \textbf{RethinkDB} para la consulta rápida y
posteriores usos.  

Si no se cumplen los requisitos de software necesarios, no podrá correr.

Garantiza, bajo la responsabilidad del usuario, la adquisición los datos de
aquellas estaciones que tengan un montaje de comunicaciones adecuado. 

Cada estación en particular debe tener disponible la entrada TCP/IP para
conectarse al \textbf{socket-server} del equipo.

El dimensionamiento del sistema se debe dar en medida de la cantidad de
estaciones que deseas adquirir en tiempo real.

Este es un manual de instalación, no la documentación para desarrolladores.

\section{Características}
\label{sec:orga138858}

\begin{itemize}
\item Lectura y decodificación de estaciones o fuentes con datos emitidos en
protocolos \{GSOF, ERYO\}
\item Almacenamiento en tiempo real de datos en RethinkDB
\item Activación de un socket server para la administración en vivo de las estaciones.
\item Sistema configurable y dimensionable según lo que se necesite
\end{itemize}

\section{Requisitos de Hardware}
\label{sec:orgabc2ce1}

Tener un servidor con las siguientes características:

\begin{itemize}
\item 8 núcleos disponibles (mínimo)
\item 64GB de ram
\item 2T de disco duro
\end{itemize}

\section{Requisitos de Software}
\label{sec:org057c85d}

Debes tener instalado lo siguiente. 

\begin{itemize}
\item Gnu/Linux \href{https://www.debian.org/}{debian} o \href{https://www.centos.org/}{Centos} (última versión)
\item Python 3.7, \url{https://www.python.org/}
\item RethinkDB 2.4, \url{https://rethinkdb.com/}
\item PostgreSQL 12, \url{https://www.postgresql.org/}
\item PostGIS, \url{https://postgis.net/}
\item GDAL-dev, \url{https://gdal.org/} (debe disponer el comando gdal-config)
\end{itemize}

Además, se necesitará los paquetes de desarrollo 

\begin{itemize}
\item postgresql-dev
\item python3-dev
\item gcc-dev
\end{itemize}

\section{Instalación}
\label{sec:org80d7182}

Una vez se tengan los requisitos de software será posible instalar el sistema \textbf{GNSS Collector} de la siguiente manera.

Utilizando el gestor de instalaciones para python \textbf{pip}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
pip install gnss_collector
\end{minted}

O bien, desde el repositorio

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
git clone https://gitlab.com/pineiden/collector.git
cd collector
python setup.py install
\end{minted}

Todo esto considerando que o estás en \textbf{ambiente virtual} o bien te aseguras que la versión de \textbf{python} es igual o superior a \textbf{3.7}.

\section{Montar Rethinkdb}
\label{sec:orgd76145d}

Una vez que se tenga instalado \textbf{Rethinkdb} se deben definir necesariamente
algunos parámetros para la correcta operación.

Dirigirse a la siguiente dirección como \textbf{root} o \textbf{sudo}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
cd /etc/rethinkdb/instances.d
\end{minted}

Crear una copia del archivo de configuración ubicada en \textbf{/etc/rethinkdb} a
\textbf{instances.d/}

Editar el archivo con al menos los siguientes parámetros

\begin{center}
\begin{tabular}{lll}
\hline
Parámetro & Valor & Comentario\\
\hline
runuser & rethinkdb & usuario\\
rungroup & rethinkdb & group\\
directory & /var/data/rdb & dónde se almacenan los datos\\
log-fle & /var/data/rdb\_log & dónde está el log\\
bind & all & habilitar acceso a la red\\
driver-port & 28015 & puerto de conexión clientes\\
http-port & 8080 & puerto de acceso interfaz administración\\
cores & 10 & cantidad de núcleos disponibles para rdb\\
cache-size & 14000 & espacio en RAM habilitado\\
io-threads & 64 & cantidad de hilos operando\\
\hline
\end{tabular}
\end{center}

Verificar correctamente la edición, con el siguiente comando podrás ver si está
bien editado (\emph{suponiendo} que tu archivo es \textbf{my\_rdb.conf}).

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
grep -v "#" my_rdb.conf|sed '/^$/d'
\end{minted}

Ahora, un paso \textbf{importante} es inicializar la \textbf{database} en la dirección
acordada en tu archivo.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
rethinkdb create -d /ruta/directorio
\end{minted}

Iniciar rethinkdb

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
systemctl start rethinkdb
\end{minted}

Como extra, algo que también se puede hacer, es tener conectadas en modo \textbf{cluster} diversas
instancias \textbf{RethinkDB}.

\section{Probar Rethinkdb}
\label{sec:orgaa94c4f}

Si necesitas verificar que la database \textbf{rethinkdb} que montaste está corriendo
bien, te recomiendo probar con el siguiente programa de prueba del módulo
\textbf{data\_rdb}

En la carpeta \textbf{tests}, el script \textbf{crea\_perfiles.py}. Si lo inicias con los
parámetros de conexión adecuados realizará algunas acciones básicas de
interacción, como 

\begin{itemize}
\item listar databases
\item crear database
\item listar tablas
\item crear tablas
\item crear index extra
\item añadir datos
\end{itemize}

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
python crea_perfiles.py
\end{minted}

Puedes clonar el módulo desde acá:

\url{https://gitlab.com/pineiden/datadbs-rethinkdb}

\section{Interfaz RethinkDB}
\label{sec:org2c843e3}

Una interfaz RethinkDB, es una plataforma web que te permite visualizar las bases de datos, tablas, escrituras y lecturas al sistema.

\begin{center}
\includegraphics[width=.9\linewidth]{./img/rdb_gui.png}
\end{center}

También, podrás realizar consultas \textbf{RQL} directamente desde la pestaña 'Data Explorer'.

\section{Crear database en postgresql}
\label{sec:orgc46b7ea}

Primero debemos crear la base de datos en postgresql.

Como root, accede al usuario postgres, así:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
su
su postgres
psql
\end{minted}

Crea el rol de usuario, con los permisos necesarios.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{sql}
create role collector;
alter role collector with superuser;
alter role collector with login;
create database collector;
alter role collector with password 'INGRESA-NUEVA-PASSWORD';
grant all privileges on database collector to collector;
\end{minted}

Conectate a la nueva database y crea las extensiones \textbf{gis}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{sql}
\c collector;
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION fuzzystrmatch;
CREATE EXTENSION address_standardizer;
CREATE EXTENSION address_standardizer_data_us;
CREATE EXTENSION postgis_tiger_geocoder;
CREATE EXTENSION postgis_sfcgal;
\end{minted}

Busca en tu sistema, en la configuración de \textbf{postgresql} el archivo
\textbf{pg\_hba.conf} 

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
find -iname "pg_hba.conf"
\end{minted}

Editalo y modifica la línea, de \textbf{peer} a \textbf{md5}:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
local  	all		all					md5
\end{minted}

Reinicia \textbf{postgresql} desde el sistema \textbf{systemctl}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
systemctl restart postgresql
\end{minted}

Con esto, para probar deberías poder acceder de la siguiente manera en tu
usuario de trabajo. Ingresando la \textbf{password que escogiste}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
psql -U collector -d collector -W
\end{minted}

\section{Crear el \textbf{schema} de \textbf{orm\_collector}}
\label{sec:org0de2f7e}

Ahora, en tu instalación de \textbf{gnss\_collector},  se habilitaron comandos para
crear el \textbf{esquema collector} en la \textbf{database} y cargar los datos.

Para crear el esquema:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
orm_create_db --help
\end{minted}

Necesitarás los datos de acceso a la database, hay dos caminos.

\subsection{Cargando los datos de ambiente}
\label{sec:orgb584bd7}

En el archivo \textbf{postactivate} de tu ambiente, poner los siguientes parámetros
para cargar al iniciar el ambiente

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
export COLLECTOR_DBUSER='collector'
export COLLECTOR_DBPASS='TU-PASSWORD-DB'
export COLLECTOR_DBNAME='collector'
export COLLECTOR_DBHOST='localhost'
export COLLECTOR_DBPORT=5432
\end{minted}

¿No lo encuentras? El siguiente comando te dará la ruta.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
find -iname "postactivate"|grep NOMBRE_AMBIENTE
\end{minted}

\subsection{Utilizando un json}
\label{sec:org9fa0723}

Crea un archivo \textbf{dbdata.json} que sea así:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{json}
{
    "dbname":"collector",
    "dbuser":"collector",
    "dbpass":"TU-PASSWORD-DB",
    "dbhost":"localhost",
    "dbport":"5432"
}
\end{minted}

\subsection{Crear el esquema.}
\label{sec:orgc777439}

Dependiendo el caso, entregando correctamente los datos de acceso.

Para datos cargados en ambiente virtual

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
orm_create_db --env
\end{minted}

Para datos en json, avisamos que no se usan los datos de ambiente y entregamos
la \textbf{ruta al archivo json}.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
orm_create_db --no-env --conf dbdata.json
\end{minted}

\section{Cargar datos a database}
\label{sec:org17f19c9}

Teniendo tu red, deberías crear los siguientes archivos \textbf{csv} con los campos
siguientes.

\begin{center}
\begin{tabular}{ll}
\hline
Archivo & Campos\\
\hline
dbtype.csv & id;typedb;name;url;data\_list\\
dbdata.csv & id;code;path;host;port;user;passw;info;dbtype;dbname\\
protocol.csv & id;name;ref;class\_name;git\\
network.csv & id;name;description\\
server.csv & id;host\_name;host\_ip;gnsocket;activated\\
station.csv & id;code;name;host;interface\_port;protocol\_host;port;ECEF\_X;ECEF\_Y;ECEF\_Z\\
(sigue) & ;db;protocol;active;server\_id;network\_id\\
\hline
\end{tabular}
\end{center}

Ver en la carpeta \textbf{ejemplos\_csv}.

En este caso, es necesario comprender que la \textbf{información} que aparezca en
\textbf{station} que está relacionada con alguna tabla anterior \textbf{debe existir} en la
tabla relacionada.

El nombre de los archivos puede cambiar si entregas un \textbf{json} con las siguientes
características.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{json}
{
    "protocol": "protocol.csv",
    "dbdata": "dbdata.csv",
    "dbtype": "dbtype.csv",
    "server": "server.csv",
    "network":"network.csv",
    "station": "station.csv"
}
\end{minted}

Y los archivos deben estar contenidos, por defecto, en la misma carpeta. Aunque
si entregas adecuadamente el \textbf{json} las rutas puedes cambiarlas. Para cargar los
datos también debes entregar la información de \textbf{dbdata}.

El comando se usa así:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
orm_load_data --help
\end{minted}

Para \textbf{GNSS Collector} el esquema será \textbf{collector}.

Si tu carpeta con datos es \textbf{./fixtures}, el comando podría ser así:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
orm_load_data --name collector --env --path fixtures
\end{minted}

Para el caso en que tomes los datos de acceso desde el ambiente y tus archivos
de datos en esa carpeta.

También, puede darse el caso de agregar datos en particular de una o pocas
estaciones extra. El comando debería poder acceder a leer los archivos de manera
ordenada y bien configurados, de ser así cargaría los nuevos datos.

\section{Crear Ambiente Virtual}
\label{sec:orgabf8500}

Para crear una ambiente virtual se debe instalar \textbf{virtualenvwrapper} 

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
pip install virtualenvwrapper --user
\end{minted}

Y asociar los siguientes parámetros a un archivo ubicado en tu \$HOME, llamémosle
\textbf{.ambiente}, en este caso para \textbf{python38} que compilé desde la fuente:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
# añadir path de instalacion de binarios de  python38 
PATH=$PATH:~/.local/bin:/opt/python38/bin
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/proyectos
# binario de python asociado al ambiente
export VIRTUALENV_PYTHON=/opt/python38/bin/python3
export VIRTUALENVWRAPPER_PYTHON=/opt/python38/bin/python3
# ruta donde está script virtualenvwrapper
source ~/.local/bin/virtualenvwrapper.sh
\end{minted}

Luego añadir a \textbf{\textasciitilde{}/.bashrc} al final.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
source ~/.ambiente
\end{minted}

Con esto, abrir nueva pestaña, crear ambiente

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
mkvirtualenv collector
\end{minted}

E instalar todo los módulos que necesites, por ejemplo:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
pip install gnss_collector
\end{minted}

Se habilitará con esto el archivo \textbf{postactivate} para cargar los parámetros de ambiente.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
~/.virtualenvs/collector/bin/postactivate
\end{minted}

\section{Parámetros en ambiente virtual}
\label{sec:orgc75b053}

Si escoges correr el sistema bajo un ambiente virtual, el archivo postactivate
debería tener, al menos lo siguiente.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
export ENV=collector
export PROJECT=CSN
# este valor coherente a la tabla server.csv que 
# activaras
export SERVER_NAME='pruebas'
# datos de base de datos
export COLLECTOR_DBUSER='collector'
export COLLECTOR_DBPASS='TU_PASSWORD_DATABASE'
export COLLECTOR_DBNAME='collector'
export COLLECTOR_DBHOST='localhost'
export COLLECTOR_DBPORT=5432
# En relación a dimensionamiento del collector
export COLLECTOR_EST_X_PROC=8
export COLLECTOR_TSLEEP=1
export COLLECTOR_WORKERS=8
export GSOF_TIMEOUT=8
export CLL_GROUP="[\"ALL\"]"
export COLLECTOR_SOCKET_IP="10.54.218.39"
export COLLECTOR_SOCKET_PORT=6677
export RDB_HOST='10.54.217.15'
export RDB_PORT=29015
export LOG_STA=true
export CLL_STATUS="ALL"
export LOG_PATH=$HOME"/collector_log"
export DT_CRITERIA=5
\end{minted}

\section{Crear JSON}
\label{sec:orga2d1fe3}

De manera análoga, si corres el sistema \textbf{GNSS Collector} sin tener un ambiente
virtual activado o los parámetros activados en ambiente, usar un \textbf{json} es buena opción.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{json}
{
  "schema":"collector",
  "cll_group": "[\"ALL\"]",
  "est_by_proc": 4,
  "tsleep": 1,
  "workers": 12,
  "gsof_timeout": 20,
  "rdb_host": "localhost",
  "rdb_port": 28015,
  "cll_status": "ALL",
  "socket_ip": "localhost_ip",
  "socket_port": 6677,
  "log_path": "~/collector_log",
  "dbdata": {
    "dbname": "nombre database",
    "dbuser": "nombre de usuario",
    "dbpass": "password para el usuario",
    "dbhost": "host de database",
    "dbport": "puerto de database"
  },
  "server_name": "collector"
}
\end{minted}

\section{Iniciar Collector}
\label{sec:org5ade1bb}
Para iniciar un sistema collector puedes realizarlo de dos maneras.

Tomando los parámetros del ambiente virtual:

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
collector --env
\end{minted}

Tomando del archivo de configuración \textbf{json}

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
collector --no-env --conf collector.json
\end{minted}

\section{El registro de eventos o LOG}
\label{sec:orgb97fa81}

Una vez que se inicia el sistema \textbf{GNSS Collector} cada evento o falla del sistema se registrará en el directorio definido por \textbf{LOG\_PATH}, modularmente hay subdirectorios para:

\begin{itemize}
\item Conexión a base de datos ORM
\item Conexión a protocolo
\item Engine (sistema collector)
\item Conexión a RethinkDB
\end{itemize}

En caso de errores es bueno consultar estos archivos y valerse de los comanodos \{ls, find, grep, awk\} para encontrar la información. 

\section{Recomendaciones para comenzar a desarrollar con GNSS Collector}
\label{sec:org0f10593}

Para quienes deseen activar o desarrollar características extras, recomiendo
crear un archivo con funciones de utilidad para la gestión de los 
valores de ambiente, archivos y módulos extras. 

Cómo base, trabajar con ambiente virtual.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
export before=$(pwd)
export FUNC_NAME=funciones.tpl.sh

function search_path(){
    cd ~
    result=$(find -iname "${FUNC_NAME}" 2>/dev/null)
    cd $before
    echo $result    
}

path=$HOME$(search_path|sed 's/^\.//g'|sed "s|/$FUNC_NAME||g")
echo "Path to file: "$path
export FN_PATH=$path

function show(){
# Mostrar las variables de ambiente
# poner path de venv postactivate:

cat $WORKON_HOME/$ENV/bin/postactivate 
}

function show_fn(){
cat $FN_PATH/$FUNC_NAME
}

function edit_fn(){
emacs $FN_PATH/$FUNC_NAME 
}

# buscar valores de estaciones 
# se ubican en carpeta fixtures
# uso
# station CODE_ESTACION
function station(){
    cd $FN_PATH
    cd ..
    path=$(pwd)
    st=$1
    if [ "${#st}" -ge "1" ]; then
    awk -F';' '{print $2,$4":"$5 ,$6, $7}' fixtures/station.csv|grep $1; 
    else
    awk -F';' '{print $2,$4":"$5 ,$6, $7}' fixtures/station.csv	
    fi
}

function edit_env(){
nano $WORKON_HOME/$ENV/bin/postactivate 
}

function save_env(){
show > ${ENV}_file
}

function pysep(){
# instalar en modo desarrollo
python setup.py develop
}

function gitignore(){
echo "Creating gitignore"
cp $FN_PATH/gitignore $(pwd)/gitignore
git add .
git commit -m "Se crea o actualiza gitignore en proyecto"
}
\end{minted}
\section{Herramientas de Utilidad}
\label{sec:org0f745f9}

\subsection{Leer directo a una estación}
\label{sec:orgb70c881}

Para leer datos directamente desde una estación o fuente de datos, mediante un protocolo disponible (GSOF, ERYO) se deben considerar los siguientes parámetros.

\begin{description}
\item[{code}] código de estación
\item[{host}] url de host de la estación
\item[{port}] puerto de conexión
\item[{limit}] cantidad de muestras
\item[{data\_file}] solo para comando protocol
\end{description}

Se dispone de los siguientes comandos:

\begin{description}
\item[{protocol}] se debe escoger protocolo y entregar los parámetros
\item[{gsof}] de uso directo, entregar los parámetros
\item[{eryo}] de uso directo, entregar los parámetros.
\end{description}

Para más información.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
protocol --help
gsof --help
eryo --help
\end{minted}

\subsection{Monitorear tabla rethinkdb}
\label{sec:orgd0ad1dd}

Si deseamos monitorear o extraer datos de una tabla en tiempo real, está disponible el comando \textbf{rdb\_monitor}, requiere los siguientes parámetros.

\begin{description}
\item[{conf}] archivo json para conectarse a un rdb
\item[{limite}] cantidad de muestras
\item[{destino}] archivo dónde se almacenan las muestras obtenidas
\item[{sleep}] tiempo de descanzo entre cada muestra, recomendable igual o superior al periodo de generación de datos.
\item[{station}] código de estación
\item[{table\_name}] nombre de tabla.
\end{description}

Para la ayuda.

\begin{minted}[frame=lines,fontsize=\scriptsize,xleftmargin=\parindent,linenos]{bash}
rdb_monitor --help
\end{minted}
\end{document}
