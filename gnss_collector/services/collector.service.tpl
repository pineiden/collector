[Unit]
Description=Collector Engine
After=network.target

[Service]
Environment="PATH=[BASE_PATH]/.virtualenvs/collector/bin"
EnvironmentFile=[BASE_PATH]/[PROJECT_PATH]/collector/gnss_collector/services/collector.env
WorkingDirectory=[BASE_PATH]/[PROJECT_PATH]/collector
RestartSec=1
ExecStart=[BASE_PATH]/.virtualenvs/collector/bin/collector
ExecStop=/bin/kill -WINCH ${MAINPID}
KillSignal=SIGCONT
Restart=always
StartLimitBurst=10

[Install]
WantedBy=multi-user.target
