Añadir nueva estacion:

Datos de dataase para esa estacion
ADMIN|ADMIN|ADD|DATADB|TEXT|\"{#info#: #gsof#, #code#: #CRSC_GSOF#, #path#: #files/CRSC_GSOF#, #name#:#TIMESERIE#, #data_list#: #[path, extension]#}\"
Datos de estacion
ADMIN|ADMIN|ADD|STA|\"{#code#:#CRSC#,#name#:#Cerro Carrasco#,#host#:#192.168.1.2#, #port#:20141, #protocol#:#GSOF#, #db#:#CRSC_GSOF#}\"
Iniciar recoleccion
ADMIN|ADMIN|INIT|VAR|STATION|2DB17124


Obtener lista de comandos:

ADMIN|ADMIN|GET|CMD

Obtener listas de otras variables

ADMIN|ADMIN|GET|LST|IDS
ADMIN|ADMIN|GET|LST|IDD
ADMIN|ADMIN|GET|LST|IPT
ADMIN|ADMIN|GET|LST|IDM
ADMIN|ADMIN|GET|LST|INST
ADMIN|ADMIN|GET|LST|DB_INST_STA
ADMIN|ADMIN|GET|LST|PROT
ADMIN|ADMIN|GET|LST|STA
ADMIN|ADMIN|GET|LST|STATUS_STA


Actualizar datos de database

ADMIN|ADMIN|UPD|DATADB|86A2|path|"giles/CRSC_GSOF_Newfolder"


Guardar en DATABASE los datos:
8AEFE50
ADMIN|ADMIN|SAVE|DB|DBDATA|ONE|9F5E
ADMIN|ADMIN|SAVE|DB|STATION|ONE|7F2AEEB3


Update values on database:

ADMIN|ADMIN|UPD|DB|STATION|94D7B70E|\"{#name#:#GPS en casa CCSN#,#host#:#10.54.218.216#}\"
ADMIN|ADMIN|UPD|DB|DATADB|[IDV]|DICT

Get data from database
ADMIN|ADMIN|GET|DB|STA
ADMIN|ADMIN|GET|DB|PROT
ADMIN|ADMIN|GET|DB|DBDATA
ADMIN|ADMIN|GET|DB|DBTYPE


