#!/bin/bash

source ~/.ambiente
collector_env=$ENV
source `which virtualenvwrapper.sh`
workon $collector_env

for i in "$@"
do
case $i in
    -l|--local)
    file_collect=local/run.py
    shift # past argument=value
    ;;
    -r|--remote)
    file_collect=remote/run.py
esac

done

BASE=/home/$USER/$PROJECT
DIR=$BASE/collector/collector
echo $DIR
cd $DIR
echo $file_collect
python $DIR/$file_collect
